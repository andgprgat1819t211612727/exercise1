﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ResetBall();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void ResetBall()
    {
        transform.position = Vector3.zero;
        Rigidbody2D rb = this.GetComponent<Rigidbody2D>();
        rb.isKinematic = true;
        rb.velocity = Vector2.zero;
        rb.isKinematic = false;
        Vector2 randVel = Random.insideUnitCircle.normalized;
        randVel.x = Mathf.Sign(randVel.x) * Mathf.Clamp(randVel.x, 0.2f, 1.0f);
        rb.velocity = randVel.normalized * 10;
    }
    
}
